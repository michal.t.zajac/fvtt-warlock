# Warlock! & Warpstar! for Foundry VTT

This official system for Foundry VTT provides character sheets and virtual
tabletop support for the Warlock! and Warpstar! role-playing games by Fire Ruby
Designs.

This system is a fan project and is not legally associated with Fire Ruby
Designs in any way.

You can purchase Warlock!
[here](https://www.drivethrurpg.com/product/312204/Warlock) and Warpstar!
[here](https://www.drivethrurpg.com/product/330123/Warpstar). You can also join
Fire Ruby Designs' Discord server [here](https://discord.com/invite/xjFgkxWEup).

## Installing the System

1. Open Foundry VTT.
2. Click the Game System tab.
3. Click Install System.
4. Search for "Warlock! & Warpstar!" in the system installation window.

## Reporting Issues, Errors, and Suggestions

File a new issue [here](https://gitlab.com/azarvel/fvtt-warlock/-/issues) or
contact me through the official Foundry VTT Discord server in the
`#other-game-systems` channel (`Azarvel#6970`).

## Credits

Content from Warlock! and Warpstar! by Greg Saunders used with permission.

Icons made by Carl Olsen, Caro Asercion, Delapouite, John Colburn, Lorc, sbed, and Skoll. Available on
https://game-icons.net.

## Acknowledgments

Thank you to Greg Saunders for writing Warlock! and Warpstar! and green-lighting
this system!

The #system-development channel on the official Foundry VTT Discord server was
the biggest lifesaver when developing this system.

The [dnd5e](https://gitlab.com/foundrynet/dnd5e) system by Atropos,
[wfrp4e](https://github.com/moo-man/WFRP4e-FoundryVTT) system by Moo Man, and
the [cyphersystem](https://github.com/mrkwnzl/cyphersystem-foundryvtt) system by
mrkwnzl were invaluable as working examples of implemented systems.