{
    "WARLOCK": {
        "ActiveEffect": {
            "NewActiveEffect": "New Active Effect",

            "AddEffect": "Add Effect",
            "ChatEffect": "Chat Effect",
            "DeleteEffect": "Delete Effect",
            "EditEffect": "Edit Effect"
        },

        "Actors": {
            "Character": {
                "Biography": "Biography",
                "Notes": "Notes",
                "Passions": "Passions",
                "Talent": "Talent"
            },

            "Monster": {
                "AdventuringSkills": "Adventuring Skills",
                "WeaponSkill": "Weapon Skill"
            },

            "Vehicle": {
                "AntiPersonnelGun": "Anti-Personnel Gun",
                "Astronav": "Astronav",
                "Manoeuvrability": "Manoeuvrability",
                "Scanners": "Scanners",
                "ShipGun": "Ship Gun",
                "Structure": "Structure",

                "CurrentStructure": "Current Structure",
                "MaximumStructure": "Maximum Structure"
            }
        },

        "Chat": {
            "Roll": {
                "Damage": "Damage Roll - {weapon} - {damageType}",
                "InitiativePlayers": "Initiative Roll - Players",
                "InitiativeGamesMaster": "Initiative Roll - Games Master",
                "PluckEvent": "Pluck Event",
                "SkillTestBasic": "Basic Skill Test - {name} - Level {level}",
                "SkillTestOpposed": "Opposed Skill Test - {name} - Level {level}",
                "StaminaLossReduction": "Stamina Loss Reduction Roll - {armour}"
            }
        },

        "CombatTracker": {
            "ModifyActionsPerRound": "Left click to increase actions remaining in the round, right click to decrease actions remaining in the round",
            "RollInitiative": "Roll initiative for each side"
        },

        "Details": {
            "Community": "Community",
            "Name": "Name",
            "Notes": "Notes",
            "Test": "Test",
            "Traits": "Traits",
            "Type": "Type"
        },

        "Dialogs": {
            "DeleteItem": {
                "Title": "Delete {item}"
            },

            "ReputationConfiguration": {
                "Title": "Reputation",
                "Description": "Description",
                "DescriptionHint": "Enter the event or group with which the reputation is associated.",

                "Cancel": "Cancel",
                "Submit": "Submit"
            },

            "SkillTest": {
                "Title": "Skill Test: {skill}",
                "Modifier": "Modifier",
                "ModifierHint": "Enter any situational modifiers to the roll.",
                "VehicleCombatCapabilities": "Vehicle Combat Capabilities",
                "VehicleCombatCapabilitiesHint": "Choose the vehicle combat capability modifier to use.",
                "CombatOptions": "Combat Options",
                "InitiatedMeleeAttack": "Initiated Melee Attack?",
                "InitiatedMeleeAttackHint": "Add 5 to the roll if you initiated a melee attack.",
                "RangedTargetFaraway": "Ranged Target Faraway?",
                "RangedTargetFarawayHint": "Subtract 5 from the roll if the target of your ranged attack is faraway.",
                "DodgingRangedAttackWithShield": "Dodging Ranged Attack with Shield?",
                "DodgingRangedAttackWithShieldHint": "Select the size of the shield, if any, to add the bonus to the roll.",
                "Pinned": "Pinned?",
                "PinnedHint": "While pinned, you subtract 5 from any tests that require you to look out from cover.",
                "Flanking": "Flanking",
                "FlankingHint": "If flanking, you add 5 to your ranged attack. If flanked, you subtract 5 from any dodge test until the start of your next round.",

                "OpposedTest": "Opposed Test",
                "Cancel": "Cancel",
                "BasicTest": "Basic Test",

                "VehicleCombatCapabilityChoices": {
                    "None": "None",
                    "ShipGun": "Ship Gun",
                    "AntiPersonnelGun":"Anti-Personnel Gun"
                },

                "ShieldChoices": {
                    "None": "None (+0)",
                    "Small": "Small (+3)",
                    "Large": "Large (+5)"
                },

                "FlankingChoices": {
                    "None": "None (+0)",
                    "Flanking": "Flanking (+5)",
                    "Flanked": "Flanked (-5)"
                }
            },

            "StaminaCost": {
                "Title": "Stamina Cost",

                "Cancel": "Cancel",
                "PayStaminaCost": "Pay Stamina Cost ({cost})"
            }
        },

        "Items": {
            "Ability": {
                "Name": "Ability"
            },

            "Armour": {
                "Name": "Armour",
                "Type": "Type",
                "ReductionRoll": "Reduction Roll",
                "Description": "Description",
                "Types": {
                    "Light": "Light",
                    "Modest": "Modest",
                    "Heavy": "Heavy"
                }
            },

            "Career": {
                "Name": "Career",
                "Skill": "Skill",
                "Current": "Current",
                "Max": "Max"
            },

            "CriticalEffect": {
                "Name": "Critical Effect"
            },

            "Equipment": {
                "Name": "Equipment",
                "Quantity": "Quantity",
                "Description": "Description"
            },

            "Glyph": {
                "Name": "Glyph",
                "StaminaCost": "Stamina Cost",
                "Test": {
                    "Basic": "Basic",
                    "Opposed": "Opposed"
                }
            },

            "Money": {
                "Name": "Money",
                "ConsolidateMoneyHint": "Consolidate money to the highest note",
                "Types": {
                    "Pennies": "Pennies",
                    "Silver": "Silver",
                    "Gold": "Gold",

                    "Frags": "Frags",
                    "Shards": "Shards",
                    "Jades": "Jades"
                }
            },

            "Spell": {
                "Name": "Spell",
                "StaminaCost": "Stamina Cost",
                "Test": {
                    "Basic": "Basic"
                }
            },

            "Weapon": {
                "Name": "Weapon",
                "Skill": "Skill",
                "Type": "Type",
                "DamageRoll": "Damage Roll",
                "DamageType": "Damage Type",
                "Description": "Description",
                "Damage": {
                    "Blast": "Blast",
                    "Crushing": "Crushing",
                    "Energy": "Energy",
                    "Piercing": "Piercing",
                    "Ship": "Ship",
                    "Slashing": "Slashing"
                },
                "Types": {
                    "Casual": "Casual",
                    "Martial": "Martial",

                    "Small": "Small",
                    "Medium": "Medium",
                    "Large": "Large"
                }
            },

            "AddItem": "Add",
            "ChatItem": "Chat Item",
            "DeleteItem": "Delete Item",
            "EditItem": "Edit Item",
            "EquipItem": "Equip Item",

            "NewItem": "New {item}"
        },

        "Notifications": {
            "MigratingWorld": "Migrating your world for version {version} — Please do not close your game or shut down your server.",
            "MigratingWorldSuccess": "Successfully migrated your world for version {version}!",
            "StaminaCost": "The stamina cost is equal to or greater than your current stamina!"
        },

        "Resources": {
            "ActionsPerRound": "Actions Per Round",
            "Advances": "Advances",
            "Luck": "Luck",
            "Pluck": "Pluck",
            "Reputation": "Reputation",
            "Stamina": "Stamina",

            "CurrentLuck": "Current Luck",
            "MaximumLuck": "Maximum Luck",

            "CurrentStamina": "Current Stamina",
            "MaximumStamina": "Maximum Stamina",

            "CurrentActionsPerRound": "Current Actions Per Round",
            "MaximumActionsPerRound": "Maximum Actions Per Round",

            "ReputationTest": "Reputation ({description})",

            "PluckTooltip": "Click to test your Pluck or shift-click to roll for an event."
        },

        "Settings": {
            "SystemMigrationVersion": "System Migration Version",
            "ActiveSystem": "Active System",
            "ActiveSystemHint": "Choose the active rules system. Note: existing actors and items must be recreated in order to function properly if this setting is changed.",
            "ActiveSystemWarlock": "Warlock!",
            "ActiveSystemWarpstar": "Warpstar!",
            "AutomaticStaminaGain": "Automatic Stamina Gain",
            "AutomaticStaminaGainHint": "Enable or disable automatically gaining stamina when the active career level increases.",
            "CareerLevelCalculation": "Career Level Calculation",
            "CareerLevelCalculationHint": "Choose the method for calculating career levels.",
            "CareerLevelCalculationLowestSkill": "Warlock!: Use the career's lowest skill as the level",
            "CareerLevelCalculationAverageSkill": "Warpstar!: Use the career's skill average as the level",
            "Pluck": "Pluck",
            "PluckHint": "Enable or disable the optional Pluck rule from Warlock! Compendium 2.",
            "Reputation": "Reputation",
            "ReputationHint": "Enable or disable the optional Reputation rule from Warlock! Compendium 2.",
            "Talent": "Talent",
            "TalentHint": "Enable or disable the optional Talent rule from Warlock! Compendium 2.",
            "Passions": "Passions",
            "PassionsHint": "Enable or disable the optional Passions rule from Warlock! Compendium 2."
        },

        "Sidebar": {
            "Settings": {
                "Blurb": "Purchase <a href='https://www.drivethrurpg.com/product/383512/Warlock-Traitors-Edition'>Warlock!</a> and <a href='https://www.drivethrurpg.com/product/330123/Warpstar'>Warpstar!</a>, and join the <a href='https://l.facebook.com/l.php?u=https%3A%2F%2Fdiscord.gg%2FxjFgkxWEup&h=AT30OjhfBQj8j2-smQCn9GjZkhiqtTeNTZi64BIuKQXneJNL-dHdV1T1cOrj0uwxhCgSOEhezycVHCXMsdbLENLe1ekb6f2OItzyGZEiM7gRpudfxIVP4CfJl7pZO2WDjZ_urIS7xzWOOc5OpbbuNQGcUyKDFkJ3EYrHA6464PPJ95eqOT5zAGuaPH3n6hN3DfIkzvs3srI34JhpGa1BRHLXoRytjE61HHZhfj-OpqeIhzC0zcB2PJCu2RCCNiVU55GNqSV69cfTSe9McmR1KB8iBIryeHksksrxWWiMr_Xd5MCeetAXaRonvZTZn6Ug_yFf177SR0EOp9vrgUzPXwvVlgHxoIzo_S40T29LUJcCYl4WlD7w7xvA9LewFnZcRLfhoYJPjgllhFZOXA77E-tAQbzdfBV1chaLxo4Ya779fUeesfGECQmPtp632HSCEbZwYh-xwM08b2ZaYlFULTIYWaDfqQOdhxAio3KEyWwchdWbKytELNJltxfd21jAPUNf7t21KHClUKaHgQCm5W0pB8CE5XARF2R4ulC1SE20dVqEUl0P9gZYLJH8D3AYipRhdImaQnTpobzTEKdv62vUGRZFgdMEHH4kIsQsV4k1-WVl0asY'>Discord community</a>!"
            }
        },

        "Skills": {
            "AnimalHandler": "Animal handler",
            "Appraise": "Appraise",
            "Astronav": "Astronav",
            "Athletics": "Athletics",
            "Bargain": "Bargain",
            "Blades": "Blades",
            "Blunt": "Blunt",
            "Bow": "Bow",
            "Brawling": "Brawling",
            "Command": "Command",
            "Crossbow": "Crossbow",
            "Diplomacy": "Diplomacy",
            "Disguise": "Disguise",
            "Dodge": "Dodge",
            "Endurance": "Endurance",
            "History": "History",
            "Incantation": "Incantation",
            "Intimidate": "Intimidate",
            "Language": "Language",
            "LargeBlade": "Large blade",
            "Lie": "Lie",
            "Medicine": "Medicine",
            "Navigation": "Navigation",
            "Ostler": "Ostler",
            "Persuasion": "Persuasion",
            "Pilot": "Pilot",
            "PoleArm": "Pole arm",
            "Repair": "Repair",
            "ShipGunner": "Ship gunner",
            "SleightOfHand": "Sleight of hand",
            "SmallArms": "Small arms",
            "SmallBlade": "Small blade",
            "Spot": "Spot",
            "Stealth": "Stealth",
            "Streetwise": "Streetwise",
            "Survival": "Survival",
            "Swimming": "Swimming",
            "Thrown": "Thrown",
            "WarpFocus": "Warp focus",
            "ZeroG": "Zero G",

            "AdventuringSkills": "Adventuring Skills",
            "WeaponSkill": "Weapon Skill"
        },

        "Tables": {
            "EquippedWeapons": "Equipped Weapons",
            "EquippedArmour": "Equipped Armour",
            "CriticalEffects": "Critical Effects",

            "Current": "Current",
            "Damage": "Damage",
            "Level": "Level",
            "Max": "Max",
            "Quantity": "Quantity",
            "Reduction": "Reduction",
            "Skill": "Skill",
            "StaminaCost": "Stamina Cost",
            "Test": "Test",
            "Type": "Type",

            "ModifyQuantityTooltip": "Left-click to increase quantity, right-click to decrease quantity",
            "PayStaminaCostTooltip": "Pay Stamina Cost",
            "RollArmourReductionTooltip": "Roll Armour Reduction",
            "RollWeaponDamageTooltip": "Roll Weapon Damage"
        },

        "Tabs": {
            "Abilities": "Abilities",
            "Combat": "Combat",
            "Description": "Description",
            "Gear": "Gear",
            "Glyphs": "Glyphs",
            "Notes": "Notes",
            "Skills": "Skills",
            "Spells": "Spells"
        },

        "Tests": {
            "TestCareer": "Test Career",
            "TestSkill": "Test Skill"
        }
    }
}
